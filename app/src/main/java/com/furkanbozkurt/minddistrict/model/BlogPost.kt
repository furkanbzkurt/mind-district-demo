package com.furkanbozkurt.minddistrict.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.furkanbozkurt.minddistrict.model.BlogPost.Companion.TABLE_NAME
import com.squareup.moshi.Json
import java.util.*

@Entity(tableName = TABLE_NAME)
data class BlogPost(
    @PrimaryKey
    var id: Int,
    var title: String = "",
    var image: String = "",
    @Json(name="short_description")
    val shortDescription: String = "",
    val description: String = ""
) {
    companion object {
        const val TABLE_NAME = "blog_post"
    }
}