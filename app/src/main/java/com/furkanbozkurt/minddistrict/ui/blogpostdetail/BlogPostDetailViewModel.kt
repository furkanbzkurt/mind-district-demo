package com.furkanbozkurt.minddistrict.ui.blogpostdetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.furkanbozkurt.minddistrict.data.repository.BlogPostRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class BlogPostDetailViewModel @Inject constructor(
    private val blogPostRepository: BlogPostRepository
) : ViewModel() {

    fun getBlogPost(blogPostId: Int) =
        blogPostRepository.getBlogPostById(blogPostId).asLiveData()
}