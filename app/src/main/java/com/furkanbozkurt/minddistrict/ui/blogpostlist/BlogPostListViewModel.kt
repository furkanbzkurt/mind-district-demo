package com.furkanbozkurt.minddistrict.ui.blogpostlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.furkanbozkurt.minddistrict.data.repository.BlogPostRepository
import com.furkanbozkurt.minddistrict.model.BlogPost
import com.furkanbozkurt.minddistrict.util.State
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject


@ExperimentalCoroutinesApi
class BlogPostListViewModel @Inject constructor(
    private val blogPostRepository: BlogPostRepository
) : ViewModel() {

    private val _blogPostsLiveData = MutableLiveData<State<List<BlogPost>>>()
    val blogPostsLiveData: LiveData<State<List<BlogPost>>>
        get() = _blogPostsLiveData


    fun getBlogPosts() {
        viewModelScope.launch {
            blogPostRepository.getBlogPosts().collect {
                _blogPostsLiveData.postValue(it)
            }
        }
    }

}