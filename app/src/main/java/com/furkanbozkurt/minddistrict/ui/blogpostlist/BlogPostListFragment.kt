package com.furkanbozkurt.minddistrict.ui.blogpostlist


import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.furkanbozkurt.minddistrict.R
import com.furkanbozkurt.minddistrict.databinding.FragmentBlogPostListBinding
import com.furkanbozkurt.minddistrict.ui.adapter.BlogPostListAdapter
import com.furkanbozkurt.minddistrict.util.*
import com.furkanbozkurt.minddistrict.util.Constants.Companion.ANIMATION_DURATION
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject


@ExperimentalCoroutinesApi
class BlogPostListFragment : Fragment(), BlogPostListAdapter.BlogPostClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvidersFactory

    @Inject
    lateinit var appContext: Context
    private lateinit var viewModel: BlogPostListViewModel
    private lateinit var binding: FragmentBlogPostListBinding
    private val mAdapter: BlogPostListAdapter by lazy {
        BlogPostListAdapter(
            blogPostClickListener = this
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        binding = FragmentBlogPostListBinding.inflate(inflater, container, false)
        context ?: return binding.root

        viewModel =
            ViewModelProvider(this, viewModelFactory).get(BlogPostListViewModel::class.java)

        initUI()
        initBlogPosts()
        handleNetworkChanges()

        return binding.root
    }

    private fun initUI() {
        binding.recyclerViewBlogPosts.adapter = mAdapter

        binding.recyclerViewBlogPosts.apply {
            postponeEnterTransition()
            viewTreeObserver.addOnPreDrawListener {
                startPostponedEnterTransition()
                true
            }
        }

        binding.recyclerViewBlogPosts.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
    }

    private fun initBlogPosts() {
        viewModel.blogPostsLiveData.observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                is State.Loading -> showLoading(true)
                is State.Success -> {
                    if (state.data.isNotEmpty()) {
                        mAdapter.submitList(state.data.toMutableList())
                        showLoading(false)
                    }
                }
                is State.Error -> {
                    showToast(state.message)
                    showLoading(false)
                }
            }
        })


        binding.swipeRefreshLayout.setOnRefreshListener {
            getBlogPosts()
        }

        if (viewModel.blogPostsLiveData.value !is State.Success) {
            getBlogPosts()
        }
    }

    private fun getBlogPosts() {
        viewModel.getBlogPosts()
    }

    private fun showLoading(isLoading: Boolean) {
        binding.swipeRefreshLayout.isRefreshing = isLoading
    }

    private fun handleNetworkChanges() {
        NetworkUtils.getNetworkLiveData(appContext)
            .observe(viewLifecycleOwner, Observer { isConnected ->
                if (!isConnected) {
                    binding.textViewNetworkStatus.text = getString(R.string.title_not_connected)
                    binding.networkStatusLayout.apply {
                        show()
                        setBackgroundColor(context.getColorRes(R.color.colorStatusNotConnected))
                    }
                } else {
                    if (viewModel.blogPostsLiveData.value is State.Error) {
                        getBlogPosts()
                    }
                    binding.textViewNetworkStatus.text = getString(R.string.title_connected)
                    binding.networkStatusLayout.apply {
                        setBackgroundColor(context.getColorRes(R.color.colorStatusConnected))

                        animate()
                            .alpha(1f)
                            .setStartDelay(ANIMATION_DURATION)
                            .setDuration(ANIMATION_DURATION)
                            .setListener(object : AnimatorListenerAdapter() {
                                override fun onAnimationEnd(animation: Animator) {
                                    hide()
                                }
                            })
                    }
                }
            })
    }

    override fun onBlogPostClick(
        blogId: Int,
        sharedImageView: ImageView,
        sharedBlogPostTitle: TextView,
        sharedBlogPostDescription: TextView
    ) {
        val extras = FragmentNavigatorExtras(
            sharedImageView to "blogPostPhoto${blogId}",
            sharedBlogPostTitle to "blogPostTitle${blogId}",
            sharedBlogPostDescription to "blogPostDescription${blogId}"
        )

        val direction =
            BlogPostListFragmentDirections.actionBlogPostDetailFragment(blogId)
        with(findNavController()) { navigate(direction, extras) }
    }

}
