package com.furkanbozkurt.minddistrict.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.furkanbozkurt.minddistrict.R
import com.furkanbozkurt.minddistrict.databinding.ItemBlogPostBinding
import com.furkanbozkurt.minddistrict.model.BlogPost

class BlogPostListAdapter(private val blogPostClickListener: BlogPostClickListener) :
    ListAdapter<BlogPost, RecyclerView.ViewHolder>(BlogPostDiffCallback()) {

    interface BlogPostClickListener {
        fun onBlogPostClick(
            blogId: Int,
            sharedImageView: ImageView,
            sharedBlogPostTitle: TextView,
            sharedBlogPostDescription: TextView
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return BlogPostViewHolder(
            ItemBlogPostBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val blogPost = getItem(position)
        (holder as BlogPostViewHolder).bind(blogPost, blogPostClickListener)
    }

    class BlogPostViewHolder(
        private val binding: ItemBlogPostBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(blogPost: BlogPost, blogPostClickListener: BlogPostClickListener) {
            binding.apply {
                this.blogPost = blogPost
                imageViewBlogPostPicture.transitionName = "blogPostPhoto${blogPost.id}"
                textViewBlogPostTitle.transitionName = "blogPostTitle${blogPost.id}"
                textViewBlogPostDescription.transitionName = "blogPostDescription${blogPost.id}"
                executePendingBindings()
            }

            binding.imageViewBlogPostPicture.load(blogPost.image) {
                placeholder(R.drawable.ic_photo)
                error(R.drawable.ic_broken_image)
            }

            binding.root.setOnClickListener {
                blogPostClickListener.onBlogPostClick(
                    blogPost.id,
                    binding.imageViewBlogPostPicture,
                    binding.textViewBlogPostTitle,
                    binding.textViewBlogPostDescription
                )
            }
        }
    }
}

private class BlogPostDiffCallback : DiffUtil.ItemCallback<BlogPost>() {

    override fun areItemsTheSame(oldItem: BlogPost, newItem: BlogPost): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: BlogPost, newItem: BlogPost): Boolean {
        return oldItem == newItem
    }
}