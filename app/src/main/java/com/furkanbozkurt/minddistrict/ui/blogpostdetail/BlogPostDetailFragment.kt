package com.furkanbozkurt.minddistrict.ui.blogpostdetail

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.core.view.forEach
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import coil.api.load
import com.furkanbozkurt.minddistrict.R
import com.furkanbozkurt.minddistrict.databinding.FragmentBlogPostDetailBinding
import com.furkanbozkurt.minddistrict.util.ViewModelProvidersFactory
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.content_blog_post_details.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class BlogPostDetailFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvidersFactory

    @Inject
    lateinit var appContext: Context
    private lateinit var viewModel: BlogPostDetailViewModel
    private lateinit var binding: FragmentBlogPostDetailBinding
    private val args: BlogPostDetailFragmentArgs by navArgs()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(R.transition.change_image_transform)
        postponeEnterTransition()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        binding = FragmentBlogPostDetailBinding.inflate(inflater, container, false)
        context ?: return binding.root

        viewModel =
            ViewModelProvider(this, viewModelFactory).get(BlogPostDetailViewModel::class.java)

        initBlogPost(args.blogPostId)
        setToolbar()

        setHasOptionsMenu(true)

        return binding.root
    }


    private fun initBlogPost(blogPostId: Int) {
        viewModel.getBlogPost(blogPostId).observe(viewLifecycleOwner, Observer { blogPost ->
            binding.blogPostContent.apply {
                textViewBlogPostTitle.transitionName = "blogPostTitle${blogPost.id}"
                textViewBlogPostTitle.text = blogPost.title
                textViewBlogPostDescription.transitionName = "blogPostDescription${blogPost.id}"
                textViewBlogPostDescription.text = blogPost.description
            }
            binding.imageViewBlogPostPicture.transitionName = "blogPostPhoto${blogPost.id}"
            binding.imageViewBlogPostPicture.load(blogPost.image)
            startPostponedEnterTransition()
        })
    }

    private fun setToolbar() {
        binding.toolbar.setNavigationOnClickListener { view ->
            view.findNavController().navigateUp()
        }
    }

}
