package com.furkanbozkurt.minddistrict.util

class Constants {
  companion object {
    const val DATABASE_NAME = "minddistrict_db"
    const val BASE_URL = "http://demo8942057.mockable.io/"
    const val ANIMATION_DURATION = 1000L
  }
}