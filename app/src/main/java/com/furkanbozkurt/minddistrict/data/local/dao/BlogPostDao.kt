package com.furkanbozkurt.minddistrict.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.furkanbozkurt.minddistrict.model.BlogPost
import kotlinx.coroutines.flow.Flow

@Dao
interface BlogPostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBlogPosts(blogPostList: List<BlogPost>)

    @Query("SELECT * FROM ${BlogPost.TABLE_NAME}")
    fun getAllBlogPosts(): Flow<List<BlogPost>>

    @Query("SELECT * FROM ${BlogPost.TABLE_NAME} WHERE id = :blogId")
    fun getBlogPostById(blogId: Int): Flow<BlogPost>
}