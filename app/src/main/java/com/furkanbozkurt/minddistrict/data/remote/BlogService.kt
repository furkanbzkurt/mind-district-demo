package com.furkanbozkurt.minddistrict.data.remote

import com.furkanbozkurt.minddistrict.model.BlogPost
import retrofit2.Response
import retrofit2.http.GET

interface BlogService {

    @GET("/minddistrict")
    suspend fun getBlogPosts(): Response<List<BlogPost>>

}