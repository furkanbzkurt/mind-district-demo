package com.furkanbozkurt.minddistrict.data.repository

import androidx.annotation.MainThread
import com.furkanbozkurt.minddistrict.data.local.dao.BlogPostDao
import com.furkanbozkurt.minddistrict.data.remote.BlogService
import com.furkanbozkurt.minddistrict.model.BlogPost
import com.furkanbozkurt.minddistrict.util.State
import com.furkanbozkurt.minddistrict.util.Utils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@ExperimentalCoroutinesApi
class BlogPostRepository @Inject constructor(
    private val blogPostDao: BlogPostDao,
    private val apiService: BlogService,
    private val utils: Utils
) {

    fun getBlogPosts(): Flow<State<List<BlogPost>>> {
        return object : NetworkBoundRepository<List<BlogPost>, List<BlogPost>>() {
            override suspend fun saveRemoteData(response: List<BlogPost>) {
                blogPostDao.insertBlogPosts(response)
            }

            override fun fetchFromLocal(): Flow<List<BlogPost>> {
                return blogPostDao.getAllBlogPosts()
            }

            override suspend fun fetchFromRemote(): Response<List<BlogPost>> {
                return apiService.getBlogPosts()
            }

        }.asFlow().flowOn(Dispatchers.IO)
    }


    @MainThread
    fun getBlogPostById(blogId: Int): Flow<BlogPost> {
        return blogPostDao.getBlogPostById(blogId)
    }

}