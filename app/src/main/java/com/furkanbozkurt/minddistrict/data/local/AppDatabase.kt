package com.furkanbozkurt.minddistrict.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.furkanbozkurt.minddistrict.data.local.dao.BlogPostDao
import com.furkanbozkurt.minddistrict.model.BlogPost
import com.furkanbozkurt.minddistrict.util.Constants

@Database(entities = [(BlogPost::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun blogPostDao(): BlogPostDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    Constants.DATABASE_NAME
                )
                    //.addMigrations(*DatabaseMigrations.MIGRATIONS)
                    .fallbackToDestructiveMigration()
                    .build()

                INSTANCE = instance
                return instance
            }
        }

    }
}