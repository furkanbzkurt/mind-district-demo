package com.furkanbozkurt.minddistrict.di.module;


import com.furkanbozkurt.minddistrict.ui.blogpostdetail.BlogPostDetailFragment;
import com.furkanbozkurt.minddistrict.ui.blogpostlist.BlogPostListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class FragmentBuilder {
    @ContributesAndroidInjector
    abstract BlogPostListFragment contributeBlogPostListFragment();

    @ContributesAndroidInjector
    abstract BlogPostDetailFragment contributeBlogPostDetailFragment();
}
