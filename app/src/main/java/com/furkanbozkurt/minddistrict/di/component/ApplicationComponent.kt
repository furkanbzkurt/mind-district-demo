package com.furkanbozkurt.minddistrict.di.component

import com.furkanbozkurt.minddistrict.MainApp
import com.furkanbozkurt.minddistrict.di.module.ActivityBuilder
import com.furkanbozkurt.minddistrict.di.module.AppModule
import com.furkanbozkurt.minddistrict.di.module.NetModule
import com.furkanbozkurt.minddistrict.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Singleton

@Singleton
@Component(
    modules = [(AndroidInjectionModule::class),
        (AndroidSupportInjectionModule::class),
        (AppModule::class),
        (ActivityBuilder::class),
        (NetModule::class),
        (ViewModelModule::class)]
)

interface ApplicationComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(mainApp: MainApp): Builder

        fun appModule(appModule: AppModule): Builder

        fun build(): ApplicationComponent
    }

    fun inject(mainApp: MainApp)
}