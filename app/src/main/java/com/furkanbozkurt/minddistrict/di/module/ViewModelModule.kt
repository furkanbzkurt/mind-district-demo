package com.furkanbozkurt.minddistrict.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.furkanbozkurt.minddistrict.di.ViewModelKey
import com.furkanbozkurt.minddistrict.ui.blogpostdetail.BlogPostDetailViewModel
import com.furkanbozkurt.minddistrict.ui.blogpostlist.BlogPostListViewModel
import com.furkanbozkurt.minddistrict.util.ViewModelProvidersFactory

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi

//@Suppress("unused")
@ExperimentalCoroutinesApi
@Module
abstract class ViewModelModule {
    /*@Binds
    @IntoMap
    @ViewModelKey(UserViewModel::class)
    abstract fun bindUserViewModel(userViewModel: UserViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun bindSearchViewModel(searchViewModel: SearchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RepoViewModel::class)
    abstract fun bindRepoViewModel(repoViewModel: RepoViewModel): ViewModel*/


    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelProvidersFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(BlogPostListViewModel::class)
    abstract fun provideBlogPostListViewModel(blogPostListViewModel: BlogPostListViewModel?): ViewModel?

    @Binds
    @IntoMap
    @ViewModelKey(BlogPostDetailViewModel::class)
    abstract fun provideBlogPostDetailViewModel(blogPostDetailViewModel: BlogPostDetailViewModel?): ViewModel?

/*    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel*/

}
