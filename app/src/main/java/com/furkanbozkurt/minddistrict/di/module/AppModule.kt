package com.furkanbozkurt.minddistrict.di.module

import android.app.Application
import android.content.Context
import com.furkanbozkurt.minddistrict.data.local.AppDatabase
import com.furkanbozkurt.minddistrict.data.local.dao.BlogPostDao
import com.furkanbozkurt.minddistrict.util.Utils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideAppContext(): Context = app

    @Provides
    @Singleton
    fun provideAppDatabase(app: Application) = AppDatabase.getInstance(app)

    @Provides
    @Singleton
    fun provideBlogPostDao(appDb: AppDatabase): BlogPostDao = appDb.blogPostDao()

    @Provides
    @Singleton
    fun provideUtils(): Utils = Utils(app)
}